﻿using System;
using System.Windows;
using System.Windows.Input;

namespace ATTUIManager.View
{
    public class ATTWindow : Window
    {
        public ATTWindow()
        {
            this.WindowStyle = WindowStyle.None;
            this.BorderThickness = new Thickness(1);
            this.KeyDown += WindowsKeyDown;
        }

        public virtual void Button_Cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public void WindowsKeyDown(object sender, KeyEventArgs e)
        {
            bool bIsUserControl = Content.GetType().BaseType.Equals(typeof(UserControls.ATTUserControl)) ? true : false;

            if (e.Key == Key.Enter)
            {
                if (bIsUserControl)
                    (Content as UserControls.ATTUserControl).EnterKeyAction();
                else
                    EnterKeyAction();
            }
            else if (e.Key == Key.Escape)
            {
                if (bIsUserControl)
                    (Content as UserControls.ATTUserControl).EscKeyAction();
                else
                    EscKeyAction();
            }
        }
        public virtual void EnterKeyAction()
        {

        }
        public virtual void EscKeyAction()
        {
            this.Close();
        }
    }
}
