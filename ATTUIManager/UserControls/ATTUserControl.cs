﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ATTUIManager.UserControls
{
    public class ATTUserControl : UserControl
    {
        public virtual void EnterKeyAction()
        {
            Console.WriteLine("User action Enter");
        }
        public virtual void EscKeyAction()
        {
            Window.GetWindow(this).Close();
        }
    }
}
