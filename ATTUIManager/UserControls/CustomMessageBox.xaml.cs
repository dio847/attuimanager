﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using ATTUIManager.View;

/// <summary>
/// Custom Message Box in WPF XAML
/// Author: Shailendra Singh Deol, 29 Feb 2016
/// License: The Code Project Open License (CPOL) 1.02
/// Introduction:
/// This tip will demonstrate how to create a custom message box in WPF.
/// Note: You need to put image files you own that are being used on messagebox for warning/Question/information/error.
/// ref https://www.codeproject.com/Tips/1081930/Custom-Message-Box-in-WPF-XAML
///
/// var messageBoxResult = CustomMessageBox.Show
/// ("Message Box Title","Are you sure?",
/// MessageBoxButton.YesNo, MessageBoxImage.Warning);
///         if (messageBoxResult != MessageBoxResult.Yes) return;
/// </summary>

namespace ATTUIManager.UserControls
{
    public enum MessageBoxType
    {
        ConfirmationWithYesNo = 0,
        ConfirmationWithYesNoCancel,
        Information,
        Error,
        Warning
    }
    public enum MessageBoxImage
    {
        Warning = 0,
        Question,
        Information,
        Error,
        Testplat,
        Editor,
        Autobot,
        Librarian,
        Visioner,
        Authorizer,
        LongBow,
        None
    }

    /// <summary>
    /// CustomMessageBox.xaml 的互動邏輯
    /// </summary>
    public partial class CustomMessageBox : ATTWindow
    {
        static object lockObj = new object();
        protected static string windowDefaultTitle = "MessageBox";

        private CustomMessageBox()
        {
            InitializeComponent();
        }
        static CustomMessageBox _messageBox;
        static MessageBoxResult _result = MessageBoxResult.No;
        public static MessageBoxResult Show
        (string caption, string msg, MessageBoxType type)
        {
            switch (type)
            {
                case MessageBoxType.ConfirmationWithYesNo:
                    return Show(caption, msg, MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                case MessageBoxType.ConfirmationWithYesNoCancel:
                    return Show(caption, msg, MessageBoxButton.YesNoCancel,
                    MessageBoxImage.Question);
                case MessageBoxType.Information:
                    return Show(caption, msg, MessageBoxButton.OK,
                    MessageBoxImage.Information);
                case MessageBoxType.Error:
                    return Show(caption, msg, MessageBoxButton.OK,
                    MessageBoxImage.Error);
                case MessageBoxType.Warning:
                    return Show(caption, msg, MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                default:
                    return MessageBoxResult.No;
            }
        }
        public static MessageBoxResult Show(string msg, MessageBoxType type)
        {
            return Show(windowDefaultTitle, msg, type);
        }
        public static MessageBoxResult Show(string msg)
        {
          return Show(windowDefaultTitle, msg,
                MessageBoxButton.OK, MessageBoxImage.None);
        }
        public static MessageBoxResult Show
        (string caption, string text)
        {
            return Show(caption, text,
            MessageBoxButton.OK, MessageBoxImage.None);
        }
        public static MessageBoxResult Show
        (string caption, string text, MessageBoxButton button)
        {
            return Show(caption, text, button,
            MessageBoxImage.None);
        }
        public static MessageBoxResult Show
        (string caption, string text,
        MessageBoxButton button, MessageBoxImage image)
        {
            lock (lockObj)
            {
                _result = MessageBoxResult.None;
                _messageBox = new CustomMessageBox
                { txtMsg = { Text = text }, MessageTitle = { Text = caption } };
                SetVisibilityOfButtons(button);
                SetImageOfMessageBox(image);
                _messageBox.ShowDialog();
                while (true)
                {
                    if (_result != MessageBoxResult.None)
                        break;
                    Thread.Sleep(50);
                }
                return _result;
            }
        }
        public static MessageBoxResult Show
        (string caption, string text,
        MessageBoxButton button, MessageBoxImage image, string yesButtonStr, string noButtonStr)
        {
            lock (lockObj)
            {
                _result = MessageBoxResult.None;
                _messageBox = new CustomMessageBox
                { txtMsg = { Text = text }, MessageTitle = { Text = caption } };
                SetPupupSelectionWindowVisibilityOfButtons(button, yesButtonStr, noButtonStr);
                SetImageOfMessageBox(image);
                _messageBox.ShowDialog();
                while (true)
                {
                    if (_result != MessageBoxResult.None)
                        break;
                    Thread.Sleep(50);
                }
                return _result;
            }

        }
        private static void SetVisibilityOfButtons(MessageBoxButton button)
        {
            switch (button)
            {
                case MessageBoxButton.OK:
                    _messageBox.btnCancel.Visibility = Visibility.Collapsed;
                    _messageBox.btnNo.Visibility = Visibility.Collapsed;
                    _messageBox.btnYes.Visibility = Visibility.Collapsed;
                    _messageBox.btnOk.Focus();
                    break;
                case MessageBoxButton.OKCancel:
                    Grid.SetColumn(_messageBox.btnOk, 2);
                    Grid.SetColumn(_messageBox.btnCancel, 3);
                    _messageBox.btnNo.Visibility = Visibility.Collapsed;
                    _messageBox.btnYes.Visibility = Visibility.Collapsed;
                    _messageBox.btnCancel.Focus();
                    break;
                case MessageBoxButton.YesNo:
                    Grid.SetColumn(_messageBox.btnYes, 2);
                    Grid.SetColumn(_messageBox.btnNo, 3);
                    _messageBox.btnOk.Visibility = Visibility.Collapsed;
                    _messageBox.btnCancel.Visibility = Visibility.Collapsed;
                    _messageBox.btnNo.Focus();
                    break;
                case MessageBoxButton.YesNoCancel:
                    Grid.SetColumn(_messageBox.btnYes, 1);
                    Grid.SetColumn(_messageBox.btnNo, 2);
                    Grid.SetColumn(_messageBox.btnCancel, 3);
                    _messageBox.btnOk.Visibility = Visibility.Collapsed;
                    _messageBox.btnCancel.Focus();
                    break;
                default:
                    break;
            }
        }
        private static void SetPupupSelectionWindowVisibilityOfButtons(MessageBoxButton button, string yesButtonStr, string noButtonStr)
        {
            _messageBox.ButtonGroupDockPanel.HorizontalAlignment = HorizontalAlignment.Stretch;
            _messageBox.btnYes.Content = yesButtonStr;
            _messageBox.btnNo.Content = noButtonStr;
            Grid.SetColumn(_messageBox.btnOk, 0);
            Grid.SetColumn(_messageBox.btnNo, 3);

            _messageBox.btnOk.Visibility = Visibility.Collapsed;
            _messageBox.btnCancel.Visibility = Visibility.Collapsed;
            _messageBox.btnNo.Focus();
        }
        private static void SetImageOfMessageBox(MessageBoxImage image)
        {
            switch (image)
            {
                // Default used AppIcon.ico
                case MessageBoxImage.Warning:
                    _messageBox.SetImage("Warning.ico");
                    break;
                case MessageBoxImage.Question:
                    _messageBox.SetImage("Question.ico");
                    break;
                case MessageBoxImage.Information:
                    _messageBox.SetImage("Information.ico");
                    break;
                case MessageBoxImage.Error:
                    _messageBox.SetImage("Error.ico");
                    break;
                case MessageBoxImage.Testplat:
                    _messageBox.SetImage("Tp-1024x1024.png");
                    break;
                case MessageBoxImage.Editor:
                    _messageBox.SetImage("Sc-1024x1024.png");
                    break;
                case MessageBoxImage.Autobot:
                    _messageBox.SetImage("At-1024x1024.png");
                    break;
                case MessageBoxImage.Librarian:
                    _messageBox.SetImage("Lb-1024x1024.png");
                    break;
                case MessageBoxImage.Visioner:
                    _messageBox.SetImage("Vs-1024x1024.png");
                    break;
                case MessageBoxImage.Authorizer:
                    _messageBox.SetImage("Ao-1024x1024.png");
                    break;
                case MessageBoxImage.LongBow:
                    _messageBox.SetImage("Ar-1024x1024.png");
                    break;
                default:
                    _messageBox.img.Visibility = Visibility.Collapsed;
                    break;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (sender == btnOk)
                _result = MessageBoxResult.OK;
            else if (sender == btnYes)
                _result = MessageBoxResult.Yes;
            else if (sender == btnNo)
                _result = MessageBoxResult.No;
            else if (sender == btnCancel)
                _result = MessageBoxResult.Cancel;
            else
                _result = MessageBoxResult.None;
            _messageBox.Close();
            _messageBox = null;
        }
        private void SetImage(string imageName)
        {
            string assmeblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            string uri = $"pack://application:,,,/{assmeblyName};component/Resource/Images/{imageName}";
            BitmapImage source = new BitmapImage(new Uri(uri, UriKind.RelativeOrAbsolute));
            if (source == null)
                return;
            img.Source = source;
        }
        public override void EscKeyAction()
        {
            this.Button_Click(btnCancel, null);
        }
    }
}
