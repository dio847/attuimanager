﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using ATTUIManager.UserControls;

namespace ATTUIManager.Basic
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }
        public bool popupSelectionResult(string content, string passString, string failString)
        {
            bool result = false;
            Application.Current.Dispatcher.Invoke(() => {
                result = (CustomMessageBox.Show("", content, MessageBoxButton.YesNo, UserControls.MessageBoxImage.Information, passString, failString) == MessageBoxResult.Yes);
            });
            return result;
        }
        object lockObj = new object();
        public void Show(string text)
        {
            lock (lockObj)
            {
                Application.Current.Dispatcher.Invoke(() => CustomMessageBox.Show("", text, MessageBoxButton.OK, UserControls.MessageBoxImage.Information));
            }
        }
    }
    public class DialogResultEventArgs : EventArgs
    {
        public bool selection { get; private set; }

        public DialogResultEventArgs(bool selection)
        {
            this.selection = selection;
        }
    }
    public class ComboBoxViewModel : ViewModelBase
    {
        public ObservableCollection<object> Options
        {
            get { return _Options; }
            set { _Options = value; }
        }
        protected ObservableCollection<object> _Options = new ObservableCollection<object>();


        public object SelectedValueOption
        {
            get { return _SelectedValueOption; }
            set
            {
                if (_SelectedValueOption != value)
                {
                    _SelectedValueOption = value;
                    OnPropertyChanged("SelectedValueOption");
                }
            }
        }
        private object _SelectedValueOption;

        public int SelectedIndexOption
        {
            get { return _SelectedIndexOption; }
            set
            {
                if (_SelectedIndexOption != value)
                {
                    _SelectedIndexOption = value;
                    OnPropertyChanged("SelectedIndexOption");
                }
            }
        }
        private int _SelectedIndexOption;
    }
}
