@ECHO OFF

set projName=ATTUIManager

set INSTALLPATH=

if exist "%programfiles(x86)%\Microsoft Visual Studio\Installer\vswhere.exe" (
  for /F "tokens=* USEBACKQ" %%F in (`"%programfiles(x86)%\Microsoft Visual Studio\Installer\vswhere.exe" -version 16.0 -property installationPath`) do set INSTALLPATH=%%F
)

call "%INSTALLPATH%\Common7\Tools\VsDevCmd.bat"
msbuild %projName%.sln /t:clean /m /nologo /v:m /p:Configuration=Release /p:Platform="x64"
msbuild %projName%.sln /t:build /m /nologo /v:m /p:Configuration=Release /p:Platform="x64"