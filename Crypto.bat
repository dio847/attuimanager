%~d0
cd %~dp0
set currentFolder=%~dp0
set ObfuscatorFolder=%~dp0bin\CryptoObfuscator_Output
set ObfuscatorObproj=ATTUIManager-Crypto
set nugetServer=http://192.168.15.50:8088/nuget
set key=3AA85796-DC4A-4F36-8840-BD08783386BA
set libName=ATTUIManager
set ReleaseFolder=%~dp0bin\Release
set proj=%currentFolder%\ATTUIManager\ATTUIManager.csproj


cd %ObfuscatorFolder%
cd ..

rmdir %ObfuscatorFolder% /s /q

C:
cd C:\Program Files (x86)\LogicNP Software\Crypto Obfuscator For .Net 2020
co.exe "projectfile=%~dp0%ObfuscatorObproj%.obproj" "assembly=%ReleaseFolder%\net461" "assembly=%ObfuscatorFolder%" encryptstrings=true

%~d0
del %ObfuscatorFolder%\*.pdb

xcopy %ReleaseFolder%\net461\*.dll %ObfuscatorFolder% /d

cd %ObfuscatorFolder%
cd ..

set app="%ObfuscatorFolder%\%libName%.dll"
FOR /F "USEBACKQ" %%F IN (`powershell -NoLogo -NoProfile -Command ^(Get-Item %app%^).VersionInfo.ProductVersion`) DO (SET fileVersion=%%F)
echo %fileVersion%

xcopy %app% %ReleaseFolder%\net461\ /y

%currentFolder%nuget.exe pack %proj% -properties Configuration=Release;Platform=x64

%currentFolder%nuget.exe delete %libName% %fileVersion% -NonInteractive %key% -Source %nugetServer%
%currentFolder%nuget.exe push %currentFolder%bin\%libName%.%fileVersion%.nupkg %key% -Source %nugetServer%


Rename CryptoObfuscator_Output Release_%fileVersion%
set ReleaseZip="Release_%fileVersion%.zip"
powershell Compress-Archive -force Release_%fileVersion%/* %ReleaseZip%
rmdir %cd%\Release_%fileVersion% /s /q

mkdir %currentFolder%\bin\Backup
set BackupFolder="%currentFolder%\bin\Backup\%fileVersion%"
mkdir %BackupFolder%
move %currentFolder%\%ObfuscatorObproj%.obproj.map %BackupFolder%
move %ReleaseZip% %BackupFolder%
